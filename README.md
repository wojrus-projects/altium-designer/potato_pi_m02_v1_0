# "POTATO_PI_M02" Single Board Computer module

**POTATO_PI_M02** jest tanim komputerem jednopłytkowym (SBC) przeznaczonym do celów R&D i hobbystycznych.

# Schemat blokowy

![](resource/sbc_block_diagram.png "SBC block diagram")

# Specyfikacja

- Procesor (MPU):
    - [STM32MP135DAE](https://www.st.com/en/microcontrollers-microprocessors/stm32mp135d.html) firmy ST Microelectronics
    - 1x rdzeń 32-bit ARM Cortex-A7
    - Taktowanie 650 lub 1000 MHz ustawiane dynamicznie
- Pamięć:
    - 512 MB DDR3L, 16 bit, 533 MHz (rozszerzalna do 1024 MB)
    - Pamięć NOR flash Quad-SPI 4 MB (rozszerzalna do 8 lub 16 MB)
    - Gniazdo karty micro SD +3.3 V (karty +1.8 V nie są obsługiwane)
- Interfejsy:
    - 2x USB 2.0 HS host
        - 2x gniazdo typu A
        - Wyjście zasilania +5.0 V / 1.1 A
        - Zasilanie sterowane przez włącznik z ograniczeniem prądowym i zwarciowym
    - 1x Ethernet 10/100 Mbps
        - Gniazdo RJ45
        - 2x LED
    - 1x port SVGA DSUB-15
        - Standard RGB444 (4096 kolorów)
        - Rozdzielczość:
            - Standardowa: VGA 640 x 480 @ 60 Hz
            - Maksymalna: WXGA 1366 x 768 @ 60 Hz
            - Pixel clock 90 MHz maks.
    - 1x złącze LCD-TFT
        - Standard RGB565 (64K kolorów)
        - Rozdzielczość maksymalna: WXGA 1366 x 768 @ 60 Hz
        - Pixel clock 90 MHz maks.
        - Dodatkowe I2C #4 do sterowania wyświetlaczem
        - Złącze IDC 2x15, raster 2.0 mm
        - Wyprowadzone zasilania +3.3 V i +5.0 V
    - 1x UART #4 diagnostyczny, header 1x3, raster 2.54mm
    - 1x złącze rozszerzeń
        - Kompatybilne z Raspberry PI 2+ (moduły HAT)
        - Header 2x20 pinów, raster 2.54 mm
        - Poziomy napięciowe CMOS +3.3 V
        - Zasilanie:
            - +3.3 V output
            - +5.0 V output/input
        - Dostępne peryferia:
            - 2x SPI (#1 i #5)
            - 2x I2C (#1 i #5)
            - 2x UART z dodatkowymi sygnałami:
                - USART #1: sygnały RTS/CTS
                - UART #8: sygnał DE (kierunek transmisji w RS-485)
            - 2x CAN-FD
            - 1x I2S (SAI #1)
            - 1x timer/PWM/clock output
- Różne:
    - Złącze SWD, header 2x5, raster 1.27 mm
    - Przycisk RESET
    - LED POWER (green)
    - LED STATUS (red)
    - 3x zworki wybierające tryb BOOT
- Zasilanie:
    - Główne +9 do +28 V DC, 1.7 A maks. (gniazdo dla wtyku okrągłego)
    - Pomocnicze +5.0 V DC, 3.0 A maks. (piny w złączu rozszerzeń)
    - Układ nadzoru stabilności zasilania +5.0 V
- PCB:
    - Wymiary 100.0 x 75.0 mm
    - Grubość 1.2 mm
    - 4 warstwy
    - Layer stackup: JLCPCB-2313-4L
    - EDA software: Altium v23.6

# Projekt PCB

Schemat: [doc/POTATO_PI_M02_V1_0_SCH.pdf](doc/POTATO_PI_M02_V1_0_SCH.pdf)

Widok 3D: [doc/POTATO_PI_M02_V1_0_3D.pdf](doc/POTATO_PI_M02_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

Rysunek montażowy: [doc/POTATO_PI_M02_V1_0_ASSEMBLY.pdf](doc/POTATO_PI_M02_V1_0_ASSEMBLY.pdf)

![](resource/sbc_pcb_3d.png "PCB 3D")

![](resource/sbc_pcb_top.png "PCB top")

![](resource/sbc_pcb_assembled.jpg "PCB assembled")

# Oprogramowanie

Przykładowe oprogramowanie jest dostępne w repozytorium [embedded/potato_pi_m02_sw](https://gitlab.com/wojrus-projects/embedded/potato_pi_m02_sw).

# Autor

Woj. Rus. <rwxrwx@interia.pl>

# Licencja

MIT
